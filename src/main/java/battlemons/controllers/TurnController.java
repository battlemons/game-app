package battlemons.controllers;


import battlemons.models.Pokemon;
import battlemons.models.Turn;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class TurnController {

    @GetMapping("/turn/move/")
    public Pokemon targetAfterMove(@RequestBody Turn turn){
        try {
            return turn.targetAfterMove();
        } catch (Exception ex) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Provide correct Turn data", ex);
        }
    }
}
