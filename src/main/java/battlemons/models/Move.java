package battlemons.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Random;

@Getter
@Setter
public class Move {
    private String description;
    private int minDamage;
    private int maxDamage;
    private Type type;
    private Random random = new Random();

    @JsonCreator
    public Move(@JsonProperty("description") String description, @JsonProperty("minDamage") int minDamage, @JsonProperty("maxDamage") int maxDamage, @JsonProperty("type") Type type){
        this.description = description;
        this.minDamage = minDamage;
        this.maxDamage = maxDamage;
        this.type = type;
    }

    public int doDamage(){
        // Get a random int between the min and max damage
        return random.nextInt((maxDamage - minDamage) + 1) + minDamage;
    }
}
