package battlemons.models;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Turn {
    private Pokemon target;
    private Move usedMove;

    @JsonCreator
    public Turn(@JsonProperty("target") Pokemon target, @JsonProperty("usedMove") Move usedMove) {
        this.target = target;
        this.usedMove = usedMove;
    }

    public Pokemon targetAfterMove(){
        useMove();

        return target;
    }

    private void useMove(){
        int dmg = (int) Math.round(usedMove.doDamage() * effectivenessModifier());
        target.takeDamage(dmg);
    }

    private double effectivenessModifier(){
       return target.getType().getType().Effectiveness(getUsedMove().getType().getType().getDescription());
    }

}
