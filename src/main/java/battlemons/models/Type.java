package battlemons.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties({ "type", "availableTypes" })
public class Type {
    private int id;
    private String description;

    @JsonIgnore
    @JsonProperty("type")
    private IType type;

    @JsonIgnore
    @JsonProperty("availableTypes")
    private List<IType> availableTypes = new ArrayList<IType>() {{
        add(new Fire());
        add(new Water());
        add(new Grass());
    }};

    public Type(@JsonProperty("id")int id, @JsonProperty("description")String description) {
        this.id = id;
        this.description = description;

        for (IType t : availableTypes) {
            if (t.getDescription().equals(description)) {
                type = t;
            }
       }
    }
}
