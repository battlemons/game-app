package battlemons.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Pokemon {
    private String name;
    private Type type;
    private List<Move> moves;
    private int hp;
    private int speed;

        @JsonCreator
        public Pokemon(@JsonProperty("name") String name, @JsonProperty("type") Type type, @JsonProperty("hp") int hp,  @JsonProperty("speed")int speed) {
            this.name = name;
            this.type = type;
            this.hp = hp;
            this.speed = speed;
        }

    public int getHp(){
        return hp;
    }

    public boolean getFainted(){
        return (hp <= 0);
    }

    public void takeDamage(int damage){
        hp -= damage;
    }
}
