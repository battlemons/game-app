package battlemons.models;

public interface IType {
    double Effectiveness(String AttackType);
    boolean isEffective();
    boolean isNotEffective();
    String getDescription();
}
