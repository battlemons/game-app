package battlemons.models;

import lombok.Getter;

@Getter
public class Water implements IType {
    private String description = "water";
    private String attackType;

    @Override
    public double Effectiveness(String AttackType) {
        attackType = AttackType;

        if (isEffective()) {
            return 1.5;
        }
        else if (isNotEffective()) {
            return 0.5;
        }
        else {
            return 1;
        }
    }

    @Override
    public boolean isEffective() {
        return (attackType.matches("grass"));
    }

    @Override
    public boolean isNotEffective() {

        return (attackType.matches("fire|water"));
    }
}