var websocket;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    websocket = new WebSocket('ws://localhost:8082/name')

    websocket.onmessage = function(data) {
        showGreeting(data.data);
    }
    setConnected(true);
}

function disconnect() {
    if (websocket != null) {
        websocket.close();
    }

    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    var data = JSON.stringify({'name': $("#name").val()})
    websocket.send(data);
}

function showGreeting(message) {
    $("#greetings").append("<tr><td> " + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
});

