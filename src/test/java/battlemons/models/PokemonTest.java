package battlemons.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class PokemonTest {
    private Pokemon testPokemon = new Pokemon("TestPokemon", new Type(3, "fire"), 100, 20);

    @Test
    public void takeDamage() {
        int damage = 30;
        int expected = 70;

        testPokemon.takeDamage(damage);

        assertTrue(testPokemon.getHp() == expected);
    }

    @Test
    public void getFaintedwhenFalse() {
        int damage = 50;
        boolean expected = false;

        testPokemon.takeDamage(damage);

        assertTrue(testPokemon.getFainted() == expected);
    }

    @Test
    public void getFaintedwhenTrue() {
        int damage = 110;
        boolean expected = true;

        testPokemon.takeDamage(damage);

        assertTrue(testPokemon.getFainted() == expected);
    }
}