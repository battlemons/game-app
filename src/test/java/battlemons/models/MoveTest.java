package battlemons.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class MoveTest {
    private Move testMove = new Move("TestMove", 30, 100, new Type(3, "Fire"));

    @Test
    public void doDamage() {
        int min = 30;
        int max = 100;


        for (int i = 0; i < 5; i++) {
            int result = testMove.doDamage();

            assertTrue("Error, damage is too low: " + result, min <= result);
            assertTrue("Error, damage is too high" + result, max >= result);
        }
    }
}