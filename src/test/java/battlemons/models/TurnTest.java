package battlemons.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class TurnTest {
    private Pokemon testPokemon = new Pokemon("testTurnPokemon", new Type(1, "fire"), 100, 20);
    private Move testMove = new Move("TestTurnMove", 10, 20, new Type(3, "grass"));
    private Turn testTurn = new Turn(testPokemon, testMove);

    @Test
    public void TargetAfterMove() {
        Pokemon result = testTurn.targetAfterMove();

        assertTrue(result.getHp() < 100);
    }

    @Test
    public void NotEffectiveMultiplier() {
        double expectedMultiplier = 0.5;

        int minExpectedHealth = 100 - (int) Math.round(testMove.getMaxDamage() * expectedMultiplier);
        int maxExpectedHealth = 100 - (int) Math.round(testMove.getMinDamage() * expectedMultiplier);

        for (int i = 0; i < 5; i++) {
            testTurn.getTarget().setHp(100);
            int result = testTurn.targetAfterMove().getHp();

            assertTrue("Error, health is too low: " + result, minExpectedHealth <= result);
            assertTrue("Error, health is too high" + result, maxExpectedHealth >= result);
        }
    }

    @Test
    public void VeryEffectiveMultiplier() {
        double expectedMultiplier = 1.5;

        testTurn.getUsedMove().setType(new Type(2, "water"));

        int minExpectedHealth = 100 - (int) Math.round(testMove.getMaxDamage() * expectedMultiplier);
        int maxExpectedHealth = 100 - (int) Math.round(testMove.getMinDamage() * expectedMultiplier);

        for (int i = 0; i < 5; i++) {
            testTurn.getTarget().setHp(100);
            int result = testTurn.targetAfterMove().getHp();

            assertTrue("Error, health is too low: " + result, minExpectedHealth <= result);
            assertTrue("Error, health is too high" + result, maxExpectedHealth >= result);
        }
    }
}